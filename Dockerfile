# Use a base image with a Linux distribution (e.g., Ubuntu)
FROM ubuntu:latest

# Install necessary dependencies
RUN apt-get update && apt-get install -y wget bzip2

# Download and install Miniconda
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda && \
    rm Miniconda3-latest-Linux-x86_64.sh

# Set the environment variables
ENV PATH="/opt/conda/bin:${PATH}"

# Update conda
RUN conda update -n base -c defaults conda

# Set the working directory
WORKDIR /app

# Create notebooks directory
RUN mkdir -p /opt/notebooks

# Start Jupyter Notebook
CMD ["jupyter", "notebook", "--notebook-dir=/opt/notebooks", "--ip='*'", "--port=8888", "--no-browser", "--allow-root"]
